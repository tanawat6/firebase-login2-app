/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  publicRuntimeConfig: {
    baseURL: process.env.NEXT_PUBLIC_API_HOST,
    environment: process.env.NEXT_PUBLIC_ENVIRONMENT || 'dev',
  },
  webpack: (config, options) => {
    config.module.rules.push({
      test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
      use: [
        options.defaultLoaders.babel,
        {
          loader: '@svgr/webpack',
          options: {
            babel: false,
            icon: true,
          },
        },
      ],
    });
    return config;
  },
  lessLoaderOptions: {
    /* ... */
    lessOptions: {
      /* ... */
      modifyVars: {
        'border-radius-base': '4px',
        'height-base': '40px',
        'modal-border-radius': '8px;',
        /* ... */
      },
    },
  },
};

module.exports = nextConfig;
