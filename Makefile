deploy:
	docker build -t asia-southeast1-docker.pkg.dev/synphaet-synphaetlife/firebase-login-poc/login2-app:latest . --platform linux/amd64
	docker push asia-southeast1-docker.pkg.dev/synphaet-synphaetlife/firebase-login-poc/login2-app:latest
	gcloud run deploy poc-firebase-login2-app --image=asia-southeast1-docker.pkg.dev/synphaet-synphaetlife/firebase-login-poc/login2-app:latest
