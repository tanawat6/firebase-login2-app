import { ReactElement, useEffect, useState } from 'react';

import Layout from 'components/Layout/Layout';
import BaseButton from 'components/Button/BaseButton';
import BaseInput from 'components/Input/BaseInput';
import { Button, Form } from 'antd';
import Card from 'components/Card/Card';
import { useRouter } from 'next/router';
import { useAuth } from 'contexts/useAuth';
import { NextPageWithLayout } from './_app';
import { useModal } from 'contexts/useModal';

const Login: NextPageWithLayout = () => {
  const [form] = Form.useForm();
  const [isSigningIn, setIsSigningIn] = useState(false);
  const { authUser, loading, signIn, verifySession } = useAuth();
  const [disabledSubmit, setDisabledSubmit] = useState(true);
  const [isLogin, setIsLogin] = useState<boolean>();
  const router = useRouter();
  const { redirect } = router.query;
  const { showError } = useModal();

  const onSubmitClick = async () => {
    const username: string = form.getFieldValue('username');
    const password: string = form.getFieldValue('password');
    const email = username.includes('@') ? username : `${username}@knot.com`;
    try {
      setIsSigningIn(true);
      console.log('onSubmitClick');
      await signIn(email, password, () => {
        if (redirect) router.push(redirect.toString());
      });
      console.log('success');
    } catch (e: any) {
      showError();
    } finally {
      setIsSigningIn(false);
    }
  };

  useEffect(() => {
    // verifySessionLogin();
  }, []);

  const verifySessionLogin = async () => {
    await verifySession(
      () => {
        // if (redirect) router.push(redirect.toString());
      },
      () => {},
    );
  };

  useEffect(() => {
    if (!loading) {
      //stay
      if (authUser) {
        setIsLogin(true);
      } else {
        setIsLogin(false);
        verifySessionLogin();
      }
    }
  }, [authUser, loading]);

  const handleFormChange = () => {
    const hasErrors = form.getFieldsError().some(({ errors }) => errors.length);
    setDisabledSubmit(hasErrors);
  };

  return (
    <div className="device__height flex justify-center items-center lg:h-[calc(100vh-64px)]">
      <Card className="w-[540px] p-8">
        <div className="relative">
          {!isLogin ? (
            <h1 className="pl-4 pt-6 text-xl font-normal">เข้าสู่ระบบ2</h1>
          ) : (
            <h1 className="pl-4 pt-6 text-xl font-normal">อยู่ในระบบ</h1>
          )}
          <div className="px-4 py-2">
            {!isLogin ? (
              <Form
                name="loginform"
                layout="vertical"
                form={form}
                onFieldsChange={handleFormChange}
                onFinish={() => onSubmitClick()}
                initialValues={{ remember: true }}
                autoComplete="off"
              >
                <Form.Item
                  name="username"
                  label={<label className="text-darkgray2">ชื่อผู้ใช้</label>}
                  help=""
                  required={false}
                  rules={[
                    {
                      required: true,
                      message: 'กรุณากรอกชื่อผู้ใช้',
                    },
                  ]}
                >
                  <BaseInput disabled={isSigningIn} />
                </Form.Item>
                <Form.Item
                  name="password"
                  required={false}
                  help=""
                  label="รหัสผ่าน"
                  rules={[
                    {
                      required: true,
                      message: 'กรุณากรอกรหัสผ่าน',
                    },
                  ]}
                >
                  <BaseInput.Password disabled={isSigningIn} />
                </Form.Item>

                <Form.Item>
                  <BaseButton
                    block
                    htmlType="submit"
                    loading={isSigningIn}
                    disabled={disabledSubmit}
                  >
                    เข้าสู่ระบบ
                  </BaseButton>
                </Form.Item>
              </Form>
            ) : (
              <span>{authUser?.name}</span>
            )}
          </div>
        </div>
      </Card>
    </div>
  );
};

Login.getLayout = function getLayout(page: ReactElement) {
  return <Layout>{page}</Layout>;
};

export default Login;
