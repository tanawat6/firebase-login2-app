import axios from 'axios.config';

interface VerifySessionResponse {
  customToken: string;
}

export interface CustomToken {
  customToken: string;
}

interface CsrfTokenResponse {
  csrfToken: string;
}

interface IAuthService {
  verifySessionToken(): Promise<CustomToken>;

  loginSession(idToken: string, csrfToken: string): Promise<void>;

  logout(): Promise<void>;

  getCsrfToken(): Promise<string>;

  checkCsrf(): Promise<void>;
}

const verifySessionToken = async (): Promise<CustomToken> => {
  const res = await axios.get<VerifySessionResponse>('/verify-session',);
  return {
    customToken: res.data.customToken,
  };
};

const loginSession = async (
  idToken: string,
  csrfToken: string,
): Promise<void> => {
  await axios.post<Response>('/login-session', {
    idToken,
    csrfToken,
    // Headers: { 'X-CSRF-TOKEN': csrfToken },
  });
};

const logout = async (): Promise<void> => {
  await axios.get<Response>('/logout');
};

const getCsrfToken = async (): Promise<string> => {
  const res = await axios.get<CsrfTokenResponse>('/csrf-token');
  return res.data.csrfToken;
};

const checkCsrf = async (): Promise<void> => {
  const res = await axios.get<Response>('/');
};

const AuthService: IAuthService = {
  verifySessionToken,
  loginSession,
  logout,
  getCsrfToken,
  checkCsrf,
};

export default AuthService;
