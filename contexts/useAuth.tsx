import {
  createContext,
  ReactNode,
  useContext,
  useEffect,
  useState,
} from 'react';
import {
  User,
  getAuth,
  signInWithCustomToken,
  signInWithEmailAndPassword,
  setPersistence,
  inMemoryPersistence,
} from 'firebase/auth';
import get from 'lodash/get';
import app from 'services/firebase';
import axios from 'axios.config';
import Cookies from 'js-cookie';
import firebase from 'firebase/app';
import AuthService from 'services/authService';
export interface AccountUser {
  uid: string;
  email: string | null;
  name: string | null;
  department: string | undefined;
  hospitalBranch: string | undefined;
  position: string | undefined;
  role: string | undefined;
}

export interface UserAttributes {
  department: string | undefined;
  hospital_branch: string | undefined;
  position: string | undefined;
  role: string | undefined;
}

const formatAuthUser = (user: User) => {
  const customAttributesJson = get(
    user,
    'reloadUserInfo.customAttributes',
    '{}',
  );
  const customAttributes = JSON.parse(customAttributesJson) as UserAttributes;
  return {
    uid: user.uid,
    email: user.email,
    name: user.displayName,
    department: customAttributes.department,
    hospitalBranch: customAttributes.hospital_branch,
    position: customAttributes.position,
    role: customAttributes.role,
  };
};

const AuthUserContext = createContext<{
  authUser: AccountUser | null;
  loading: boolean;
  signIn: (
    email: string,
    password: string,
    onSessionValid: () => void,
  ) => Promise<void>;
  signOut: () => Promise<void>;
  verifySession: (
    onSessionValid: () => void,
    onSessionInvalid: () => void,
  ) => Promise<void>;
}>({
  authUser: null,
  loading: true,
  signIn: async () => {},
  signOut: async () => {},
  verifySession: async () => {},
});

export function AuthUserProvider({ children }: { children: ReactNode }) {
  const [authUser, setAuthUser] = useState<AccountUser | null>(null);
  const [loading, setLoading] = useState(true);
  const auth = getAuth(app);

  const initAxiosHeader = (token: string) => {
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
  };

  const initCsrfHeader = (csrf: string | undefined) => {
    axios.defaults.headers.common['X-CSRF-TOKEN'] = csrf;
  };

  const authStateChanged = async (authState: User | null) => {
    if (!authState) {
      setAuthUser(null);
      setLoading(false);
      return;
    }
    setLoading(true);
    const token = await auth.currentUser?.getIdToken();
    if (token) {
      await initAxiosHeader(token);
    }
    const formattedUser = formatAuthUser(authState);
    setAuthUser(formattedUser);
    setLoading(false);
  };

  const getCookie = (name: string) => {
    const v = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
    return v ? v[2] : undefined;
  };

  const verifySession = async (
    onSessionValid: () => void,
    onSessionInvalid: () => void,
  ) => {
    //check session cookies
    // const sessionId = Cookies.get('session');
    // const csrf = Cookies.get('csrf_token');

    // console.log(`sessionId ${sessionId}`);
    // if (sessionId) {
    // setPersistence(auth, inMemoryPersistence);
    // Send the CSRF token with authenticated requests.
    // await initCsrfHeader(csrf);

    // Verify the session on each page load.
    try {
      const customToken = (
        await AuthService.verifySessionToken()
      ).customToken;
      console.log(`verifySessionToken customToken ${customToken}`);
      await signInWithCustomToken(auth, customToken)
        .then(({ user }) => {
          console.log(`signInWithCustomToken user ${user.uid}`);
          onSessionValid();
        })
        .catch(error => {
          console.log(`signInWithCustomToken error ${error}`);
          // The session is invalid.
          // Redirect to login page.
          onSessionInvalid();
        });
    } catch (e) {
      // The session is invalid.
      // Redirect to login page.
      console.log(`session is invalid. error ${e}`);
      onSessionInvalid();
    }
    // } else {
    //   console.log(`no session`);
    // }
  };

  const signIn = async (
    email: string,
    password: string,
    onSessionValid: () => void,
  ) => {
    const value = `; ${document.cookie}`;
    console.log('document.cookie ' + value);
    console.log('signIn function');
    setPersistence(auth, inMemoryPersistence);
    // auth.setPersistence(firebase.auth.Auth.Persistence.NONE);
    console.log('setPersistence NONE');
    signInWithEmailAndPassword(auth, email, password)
      .then(({ user }) => {
        console.log('signInWithEmailAndPassword DONE');
        return user.getIdToken().then(async idToken => {
          console.log('getIdToken ' + idToken);
          try {
            await AuthService.checkCsrf();
            const csrf = getCookie('csrfToken');
            console.log('csrf ' + csrf);
            await AuthService.loginSession(idToken, csrf ? csrf : '');
            onSessionValid();
            console.log('loginSession success');
          } catch (e) {
            console.log('loginSession failed ' + e);
            auth.signOut();
          }
        });
      })
      .catch(() => {
        console.log('getIsignInWithEmailAndPasswordToken error');
        auth.signOut();
      });
  };

  const signOut = async () => {
    // await auth.signOut().then( async () => {
    console.log('logout');
    try {
      const csrf = getCookie('csrf_token');
      await initCsrfHeader(csrf);
      await AuthService.logout();
      console.log('logout success');
      await auth.signOut();
    } catch {
      console.log('logout failed');
    }
    // });
  };

  // listen for Firebase state change
  useEffect(() => {
    const unsubscribe = auth.onAuthStateChanged(authStateChanged);
    return () => unsubscribe();
  }, []);

  return (
    <AuthUserContext.Provider
      value={{
        authUser,
        loading,
        signIn,
        signOut,
        verifySession,
      }}
    >
      {children}
    </AuthUserContext.Provider>
  );
}
// custom hook to use the authUserContext and access authUser and loading
export const useAuth = () => useContext(AuthUserContext);
