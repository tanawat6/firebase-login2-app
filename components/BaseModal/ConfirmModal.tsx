import { ModalProps } from 'antd';
import BaseButton from '../Button/BaseButton';
import BaseModal, { BaseModalProps } from './BaseModal';

interface ConfirmModalProps {
  onOk?: () => void;
  onCancel?: () => void;
  visible?: boolean;
  msg?: string;
  cancelButtonName?: string;
  okButtonName?: string;
  danger?: boolean;
  fontType?: 'admin' | 'line';
  singleButton?: boolean;
  showMsg?: boolean;
  loading?: boolean;
}

const ConfirmModal: React.FC<
  ModalProps & BaseModalProps & ConfirmModalProps
> = ({
  onOk,
  onCancel,
  visible,
  title,
  msg,
  cancelButtonName,
  okButtonName,
  danger = false,
  singleButton = false,
  showMsg = true,
  loading,
  ...props
}) => {
  const cancelButton = cancelButtonName ? cancelButtonName : 'ยกเลิก';
  const okButton = okButtonName ? okButtonName : 'ยืนยัน';
  const message = msg ? msg : 'กดยืนยันเพื่อดำเนินการต่อ';
  return (
    <BaseModal onCancel={onCancel} visible={visible} {...props}>
      <div>
        <div className="flex flex-col justify-center items-center shadow px-6 md:px-[50px] py-[40px] bg-white rounded-[4px]">
          <img className="h-[48px] w-[48px]" src="/circle-alert-fill.png" />
          <span
            className={`text-xl whitespace-pre-wrap ${
              danger ? 'text-error' : 'text-primary'
            } mt-2 text-center`}
          >
            {title}
          </span>
          {showMsg && (
            <span className="text-center whitespace-pre-wrap">{message}</span>
          )}
          <div className="flex flex-col md:flex-row justify-around mt-8 w-full">
            {!singleButton && (
              <BaseButton
                type="default"
                block
                className="md:mr-2 min-w-[0px] md:min-w-[160px] md:order-first order-last mt-4 md:mt-0"
                danger={danger}
                onClick={onCancel}
                disabled={loading}
              >
                {cancelButton}
              </BaseButton>
            )}
            <BaseButton
              type="primary"
              className={`${
                !singleButton && 'md:ml-2'
              } min-w-[0px] md:min-w-[160px] md:order-last order-first`}
              block
              danger={danger}
              onClick={onOk}
              loading={loading}
            >
              {okButton}
            </BaseButton>
          </div>
        </div>
      </div>
    </BaseModal>
  );
};
export default ConfirmModal;
