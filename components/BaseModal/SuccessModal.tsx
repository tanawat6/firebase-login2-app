import { ModalProps } from 'antd';
import BaseModal, { BaseModalProps } from './BaseModal';

interface SuccessModalProps {
  onClose?: () => void;
  visible?: boolean;
  msg?: string;
  title?: string;
  children?: React.ReactNode;
}

const SuccessModal: React.FC<
  ModalProps & BaseModalProps & SuccessModalProps
> = ({ onClose, visible, title, msg, children, ...props }) => {
  return (
    <BaseModal onCancel={onClose} visible={visible} {...props}>
      <div>
        <div className="flex flex-col justify-center items-center shadow px-[50px] py-[40px] bg-white rounded-[4px]">
          <img className="h-[48px] w-[48px]" src="/circle-success-fill.png" />
          <span
            className={
              'text-xl text-center text-primary mt-2  whitespace-pre-wrap'
            }
          >
            {title}
          </span>
          {msg && (
            <span className="text-center mt-2  whitespace-pre-wrap">{msg}</span>
          )}
          {children}
        </div>
      </div>
    </BaseModal>
  );
};
export default SuccessModal;
