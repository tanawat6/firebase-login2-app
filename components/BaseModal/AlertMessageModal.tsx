import { ModalProps } from 'antd';
import BaseModal, { BaseModalProps } from './BaseModal';

interface ErrorModalProps {
  onClose?: () => void;
  visible?: boolean;
  msg?: string;
  danger?: boolean;
  children?: React.ReactNode;
}

const AlertMessageModal: React.FC<
  ModalProps & BaseModalProps & ErrorModalProps
> = ({ onClose, visible, title, msg, danger = false, children, ...props }) => {
  return (
    <BaseModal onCancel={onClose} visible={visible} {...props}>
      <div className="flex flex-col justify-center items-center shadow bg-white !w-[calc(100vw-30px)] rounded-[4px] px-[50px] py-[40px] md:w-[504px] md:max-w-[504px]">
        <img className="h-[48px] w-[48px]" src="/circle-alert-fill.png" />
        <span
          className={`text-xl text-center  whitespace-pre-wrap ${
            danger ? 'text-error' : 'text-primary'
          } mt-2`}
        >
          {title}
        </span>
        {msg && (
          <span className="text-center mt-2  whitespace-pre-wrap">{msg}</span>
        )}
        {children}
      </div>
    </BaseModal>
  );
};
export default AlertMessageModal;
