import { Modal, ModalProps } from 'antd';

export interface BaseModalProps {
  children?: React.ReactNode;
  className?: string;
  fontType?: 'admin' | 'line';
}

const BaseModal: React.FC<ModalProps & BaseModalProps> = ({
  children,
  className,
  fontType = 'admin',
  ...props
}) => {
  return (
    <Modal
      className={`${fontType == 'admin' ? 'admin-modal' : ''} ` + className}
      bodyStyle={{ padding: 0 }}
      centered
      footer={null}
      {...props}
    >
      {children}
    </Modal>
  );
};
export default BaseModal;
