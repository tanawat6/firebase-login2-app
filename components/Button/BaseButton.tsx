import { Button } from 'antd';
import { MouseEventHandler } from 'react';

interface ButtonProps {
  children: React.ReactNode;
  type?: 'primary' | 'text' | 'default';
  block?: boolean;
  disabled?: boolean;
  danger?: boolean;
  className?: string;
  icon?: React.ReactNode;
  htmlType?: 'button' | 'submit' | 'reset';
  size?: 'large';
  onClick?: MouseEventHandler;
  loading?: boolean;
}

const BaseButton: React.FC<ButtonProps> = ({
  children,
  type = 'primary',
  onClick,
  block,
  disabled,
  className,
  danger,
  htmlType,
  icon,
  size,
  loading,
  ...props
}) => {
  let disabledState = '';
  switch (type) {
    case 'default':
      if (danger) {
        disabledState =
          'disabled:!text-gray2 disabled:!border-gray2 disabled:!bg-white !text-black !border-gray1  hover:!text-error hover:!border-error focus:!text-error focus:!border-error';
      } else {
        disabledState =
          'disabled:!text-gray2 disabled:!border-gray2 disabled:!bg-white !text-primary !border-primary hover:!bg-primary-light';
      }
      break;
    default:
      disabledState =
        'disabled:!text-gray1 disabled:!border-gray2 disabled:!bg-gray2';
  }
  return (
    <Button
      type={type}
      block={block}
      onClick={onClick}
      disabled={disabled}
      className={`min-w-[160px] text-base font-normal ${disabledState} ${
        className ? className : ''
      }`}
      htmlType={htmlType}
      danger={danger}
      icon={icon}
      size={size}
      loading={loading}
      {...props}
    >
      {children}
    </Button>
  );
};
export default BaseButton;
