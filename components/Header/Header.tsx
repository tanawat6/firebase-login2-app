import { Button } from 'antd';
import { useAuth } from 'contexts/useAuth';

const getLogoUrl = (branchId: string): string => {
  return `https://storage.googleapis.com/prod-synphaet-synphaetlife-assets/logo-${branchId}.png`;
};

const Header: React.FC = () => {
  const { authUser, signOut } = useAuth();
  return (
    <div className="sticky top-0 z-50 bg-primary w-full flex items-center justify-between h-[64px]">
      {<img src={getLogoUrl('default')} className="h-12 ml-3" />}
      {authUser && (
        <div className="mr-3">
          <span className="mr-2 text-white">{authUser.name}</span>
          <Button onClick={signOut} size="small">
            logout
          </Button>
        </div>
      )}
    </div>
  );
};

export default Header;
