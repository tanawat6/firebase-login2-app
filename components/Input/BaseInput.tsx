import { Input, InputProps } from 'antd';

interface BaseInputProps {}

const BaseInput = ({ className, ...props }: BaseInputProps & InputProps) => {
  return <Input className={'!rounded ' + className} {...props} />;
};

BaseInput.Password = Input.Password;

export default BaseInput;
